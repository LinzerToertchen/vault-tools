#!/usr/bin/env bash

RECIPIENTS=""

while read recipient; do
  RECIPIENTS+=" -r \"${recipient}\""
done <vault/.gpg-id

gpg --use-agent --decrypt vault/vault_passphrase.gpg | eval gpg --batch --yes -e $RECIPIENTS -o vault/vault_passphrase.gpg
